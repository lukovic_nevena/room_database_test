package com.example.nevena.foursqaretest.entity;

/**
 * Created by Nevena on 12/12/2016.
 */

public class Icon extends BaseEntity{
    private String prefix;
    private String suffix;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
