package com.example.nevena.foursqaretest.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.nevena.foursqaretest.entity.Category;
import com.example.nevena.foursqaretest.entity.Venue;

/**
 * Created by Sinan Dizdarevic on 9.11.17..
 */

@Database(entities = {Venue.class, Category.class}, version = 1)
public abstract class SimpleDatabaseClass extends RoomDatabase {
    public abstract VenueDao venueDao();
    public abstract CategoryDao categoryDao();
    public abstract VenueCategoryDao venueCategoryDao();
}
