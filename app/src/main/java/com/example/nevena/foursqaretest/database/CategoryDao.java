package com.example.nevena.foursqaretest.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.nevena.foursqaretest.entity.Category;

import java.util.List;

/**
 * Created by Sinan Dizdarevic on 13.11.17..
 */

@Dao
public interface CategoryDao {
    @Query("SELECT * FROM category where venueId LIKE :venueId")
    List<Category> loadCategories(String venueId);

    @Query("SELECT * FROM category")
    List<Category> loadAllCategories();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Category> categories);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Category category);
}
