package com.example.nevena.foursqaretest.api;

import com.example.nevena.foursqaretest.listener.SearchListener;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nevena on 12/12/2016.
 */

public class RequestManager {

    public static void search(String search) {
        Map<String, String> params = new HashMap<>();
        params.put("client_id", "PWSM1K5Y4FPYRQNBN5VTV00TA5PBIRGNQ4GOX1BJOVIEH5RX ");
        params.put("client_secret", "14CLKGBEMGHFAAC2W4UFJM3INJILSDTPEANMU3NYMJQAFRUH");
        params.put("query", search);
        params.put("ll", "40.7,-74");
        params.put("v", "20161212");
        params.put("m", "foursquare");

        RestClient.INSTANCE.getApiService().search(params).enqueue(new SearchListener());
    }

    public static void search(double lat, double lng) {
        Map<String, String> params = new HashMap<>();
        params.put("client_id", "PWSM1K5Y4FPYRQNBN5VTV00TA5PBIRGNQ4GOX1BJOVIEH5RX ");
        params.put("client_secret", "14CLKGBEMGHFAAC2W4UFJM3INJILSDTPEANMU3NYMJQAFRUH");
        params.put("ll", lat + "," + lng);
        params.put("v", "20161212");
        params.put("m", "foursquare");
        params.put("query", "beer");
        RestClient.INSTANCE.getApiService().search(params).enqueue(new SearchListener());
    }
}
