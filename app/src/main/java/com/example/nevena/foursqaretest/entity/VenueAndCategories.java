package com.example.nevena.foursqaretest.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

/**
 * Created by Sinan Dizdarevic on 9.11.17..
 */

public class VenueAndCategories {
    @Embedded
    public Venue venue;

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    @Relation(parentColumn = "id", entityColumn = "venueId")
    public List<Category> categories;
}
