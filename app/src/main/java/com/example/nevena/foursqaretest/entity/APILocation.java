package com.example.nevena.foursqaretest.entity;

/**
 * Created by Nevena on 12/12/2016.
 */

//@Entity(tableName = "location")
public class APILocation extends BaseEntity {
//    @PrimaryKey(autoGenerate = true)
//    private int locationId;
//    @Embedded
    private String address;
//    @ColumnInfo(name = "lat")
    private double lat;
//    @ColumnInfo(name = "lng")
    private double lng;

//    public int getLocationId() {
//        return locationId;
//    }
//
//    public void setLocationId(int locationId) {
//        this.locationId = locationId;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
