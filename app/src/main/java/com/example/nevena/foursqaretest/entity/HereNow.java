package com.example.nevena.foursqaretest.entity;

/**
 * Created by Nevena on 12/12/2016.
 */

public class HereNow extends BaseEntity{
    private  int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
