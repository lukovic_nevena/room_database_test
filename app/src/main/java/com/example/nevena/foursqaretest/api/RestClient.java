package com.example.nevena.foursqaretest.api;

/**
 * Created by Nevena on 12/12/2016.
 */

public enum RestClient {
    INSTANCE;

    private ApiService service;

    public ApiService getApiService() {
        if (service == null) {
            service = ServiceGenerator.createService(ApiService.class, "https://api.foursquare.com/v2/");
        }
        return service;
    }
}