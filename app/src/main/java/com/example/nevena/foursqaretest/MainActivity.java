package com.example.nevena.foursqaretest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.btn_search)
    Button btnSearch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_search)
    public void onButtonSearchClick() {
        Intent intent = new Intent(this, ListActivity.class);
        if (!etSearch.getText().toString().isEmpty()) {
            intent.putExtra("search", etSearch.getText().toString());
        }
        startActivity(intent);
    }
}
