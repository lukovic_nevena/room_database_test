package com.example.nevena.foursqaretest.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;

import com.example.nevena.foursqaretest.entity.VenueAndCategories;

import java.util.List;

/**
 * Created by Sinan Dizdarevic on 14.11.17..
 */

@Dao
public interface VenueCategoryDao {
    @Transaction
    @Query("SELECT * from venue")
    public List<VenueAndCategories> loadVenueAndCategories();

}
