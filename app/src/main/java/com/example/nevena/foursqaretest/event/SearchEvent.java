package com.example.nevena.foursqaretest.event;

import com.example.nevena.foursqaretest.entity.APIResponse;

import retrofit2.Response;

/**
 * Created by Nevena on 12/12/2016.
 */

public class SearchEvent {
    private Response<APIResponse> response;

    public Response<APIResponse> getResponse() {
        return response;
    }

    public void setResponse(Response<APIResponse> response) {
        this.response = response;
    }

    public SearchEvent(Response<APIResponse> response) {
        this.response = response;
    }
}
