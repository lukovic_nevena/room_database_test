package com.example.nevena.foursqaretest;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.example.nevena.foursqaretest.database.SimpleDatabaseClass;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Nevena on 12/12/2016.
 */

public class FourSquareApp extends Application {

    public static FourSquareApp INSTANCE;
    private static final String DATABASE_NAME = "SimpleDatabase";
    private static final String PREFERENVCES = "FourSqareTest.preferences";

    private SimpleDatabaseClass database;

    public static FourSquareApp get() {
        return INSTANCE;
    }

    public static EventBus getEventBus() {
        return EventBus.getDefault();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(), SimpleDatabaseClass.class, DATABASE_NAME)
                .build();
        INSTANCE = this;

        Logger.addLogAdapter(new AndroidLogAdapter());
    }

    public SimpleDatabaseClass getDatabase() {
        return database;
    }
}
