package com.example.nevena.foursqaretest.entity;

import java.util.List;

/**
 * Created by Nevena on 12/12/2016.
 */

public class APIResponse2 extends BaseEntity{
private List<Venue> venues;

    public List<Venue> getVenues() {
        return venues;
    }

    public void setVenues(List<Venue> venues) {
        this.venues = venues;
    }
}
