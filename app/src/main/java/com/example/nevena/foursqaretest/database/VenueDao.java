package com.example.nevena.foursqaretest.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.nevena.foursqaretest.entity.Category;
import com.example.nevena.foursqaretest.entity.Venue;

import java.util.List;

/**
 * Created by Sinan Dizdarevic on 9.11.17..
 */

@Dao
public interface VenueDao {

    @Query(" SELECT * FROM venue")
    List<Venue> getAll();

    @Query("SELECT * FROM venue WHERE name LIKE '%'||:name||'%'")
    List<Venue> findByName(String name);

    @Insert
    void insert(Venue venue);

    @Insert
    void insertAll(List<Venue> venues);

    @Insert
    void insertVenueAndCategories(Venue venue, List<Category> categories);

    @Update
    void update(Venue venue);

    @Delete
    void delete(Venue venue);
}
