package com.example.nevena.foursqaretest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nevena.foursqaretest.R;
import com.example.nevena.foursqaretest.entity.VenueAndCategories;
import com.example.nevena.foursqaretest.listener.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nevena on 12/12/2016.
 */

public class BarsAdapter extends RecyclerView.Adapter<BarsAdapter.BarsHolder> {

    private Context context;
    private List<VenueAndCategories> venuesAndCategories;
    private OnItemClickListener mItemClickListener;

    public BarsAdapter(Context context, List<VenueAndCategories> venuesAndCategories, OnItemClickListener onItemClickListener) {
        this.context = context;
        this.venuesAndCategories = venuesAndCategories;
        this.mItemClickListener= onItemClickListener;
    }

    @Override
    public BarsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        BarsHolder holder = new BarsHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(BarsHolder holder, int position) {
        VenueAndCategories venueAndCategories = venuesAndCategories.get(position);
        String iconUrl = venueAndCategories.getCategories().get(0).getIcon().getPrefix()+"64" + venueAndCategories.getCategories().get(0).getIcon().getSuffix();
        Picasso.with(context).load(iconUrl).into(holder.ivIcon);
        holder.tvName.setText(venueAndCategories.getVenue().getName());
        holder.tvAddress.setText(venueAndCategories.getVenue().getLocation().getAddress());
    }

    @Override
    public int getItemCount() {
        return venuesAndCategories.size();
    }

    public class BarsHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_icon)
        ImageView ivIcon;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_address)
        TextView tvAddress;

        public BarsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mItemClickListener != null){
                        mItemClickListener.onItemClick(BarsHolder.this.getAdapterPosition());
                    }
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if(mItemClickListener != null){
                        mItemClickListener.onItemLongClick(BarsHolder.this.getAdapterPosition());
                    }
                    return false;
                }
            });
        }
    }
}
