package com.example.nevena.foursqaretest.listener;

/**
 * Created by Sinan Dizdarevic on 9.11.17..
 */

public interface OnItemClickListener {
    void onItemClick(int position);
    void onItemLongClick(int position);
}
