package com.example.nevena.foursqaretest.api;

import com.example.nevena.foursqaretest.entity.APIResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Nevena on 12/12/2016.
 */

public interface ApiService {
    @GET("venues/search")
    Call<APIResponse> search(@QueryMap Map<String, String> params);
}
