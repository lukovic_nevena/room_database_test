package com.example.nevena.foursqaretest.listener;

import com.example.nevena.foursqaretest.FourSquareApp;
import com.example.nevena.foursqaretest.entity.APIResponse;
import com.example.nevena.foursqaretest.event.SearchEvent;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Nevena on 12/12/2016.
 */

public class SearchListener implements Callback<APIResponse> {
    @Override
    public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
        FourSquareApp.getEventBus().post(new SearchEvent(response));
    }

    @Override
    public void onFailure(Call<APIResponse> call, Throwable t) {
        t.printStackTrace();
    }
}
