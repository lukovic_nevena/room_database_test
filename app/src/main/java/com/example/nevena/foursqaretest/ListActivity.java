package com.example.nevena.foursqaretest;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.nevena.foursqaretest.adapter.BarsAdapter;
import com.example.nevena.foursqaretest.api.RequestManager;
import com.example.nevena.foursqaretest.entity.Category;
import com.example.nevena.foursqaretest.entity.Venue;
import com.example.nevena.foursqaretest.entity.VenueAndCategories;
import com.example.nevena.foursqaretest.event.SearchEvent;
import com.example.nevena.foursqaretest.listener.OnItemClickListener;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListActivity extends AppCompatActivity implements OnItemClickListener {

    private BarsAdapter barsAdapter;
    private List<VenueAndCategories> venuesAndCategories;
    @BindView(R.id.rv_items)
    RecyclerView rvItems;

    @Override
    protected void onStart() {
        super.onStart();
        FourSquareApp.getEventBus().register(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        FourSquareApp.getEventBus().unregister(this);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        ButterKnife.bind(this);
        venuesAndCategories = new ArrayList<>();
        barsAdapter = new BarsAdapter(this, venuesAndCategories, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvItems.setAdapter(barsAdapter);
        rvItems.setLayoutManager(layoutManager);

//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                String search;
//                venuesAndCategories.addAll(FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories());
//                for (int i =0; i < FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().size(); i++) {
//                    Logger.d("Category size " + FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories().size());
//                  if (FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories() != null) {
//                      Logger.d("Category true ");
//                  } else {
//                      Logger.d("Category false ");
//                  }
//                }
//
//                if (venuesAndCategories != null && venuesAndCategories.size() > 0) {
////                    Custom SELECT
////                    venues.clear();
////                    venues.addAll(FourSquareApp.get().getDatabase().venueDao().findByName("Blue"));
//                    Logger.d("Category id " + venuesAndCategories.get(0).getCategories().get(0).getName());
//                    barsAdapter.notifyDataSetChanged();
//                } else if (getIntent() != null && getIntent().getExtras() != null) {
//                    search = getIntent().getExtras().getString("search");
//                    RequestManager.search(search);
//                } else {
//                    RequestManager.search(40.7, -74);
//                }
//            }
//
//        });

        new Async().execute();

    }

    @Subscribe
    public void onSearchEvent(final SearchEvent event) {
        for (int i = 0; i < event.getResponse().body().getResponse().getVenues().size(); i++) {
            VenueAndCategories venueAndCategories = new VenueAndCategories();
            venueAndCategories.setVenue(event.getResponse().body().getResponse().getVenues().get(i));
            venueAndCategories.setCategories(event.getResponse().body().getResponse().getVenues().get(i).getCategories());
            venuesAndCategories.add(venueAndCategories);
        }
        barsAdapter.notifyDataSetChanged();
        writeVenues(event.getResponse().body().getResponse().getVenues());

    }

//    private void writeVenues(final Venue venue, final List<Category> categories) {
//        // Insert into database
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                FourSquareApp.get().getDatabase().venueDao().insert(venue);
//                for (int i = 0; i < categories.size(); i++) {
//                    categories.get(i).setVenueId(venue.getId());
//                }
//                FourSquareApp.get().getDatabase().categoryDao().insertAll(categories);
//            }
//        });
//    }

    private void writeVenues(final List<Venue> venues) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < venues.size(); i++) {
                    FourSquareApp.get().getDatabase().venueDao().insert(venues.get(i));
                    for (int j = 0; j < venues.get(i).getCategories().size(); j++) {
                        Category category = venues.get(i).getCategories().get(j);
                        category.setVenueId(venues.get(i).getId());
                        FourSquareApp.get().getDatabase().categoryDao().insert(category);
                    }
                    Logger.d(FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories().get(0).getName() + " " + FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories().get(0).getVenueId());

                }
                for (int i =0; i < FourSquareApp.get().getDatabase().categoryDao().loadAllCategories().size(); i++) {
                    Category category = FourSquareApp.get().getDatabase().categoryDao().loadAllCategories().get(i);
                    Logger.d("Category " + category.getName() + " " + category.getVenueId());
                }
            }
        });
    }

//    private void writeVenuesAndCategories(final List<VenueAndCategories> venuesAndCategories) {
//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                FourSquareApp.get().getDatabase().venueCategoryDao().insertAll(venuesAndCategories);
//            }
//        });
//
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(ListActivity.this, DetailsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemLongClick(final int position) {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                FourSquareApp.get().getDatabase().venueDao().delete(venuesAndCategories.get(position).getVenue());
                Logger.d("Category size " + FourSquareApp.get().getDatabase().categoryDao().loadAllCategories().size());

            }
        });
        barsAdapter.notifyDataSetChanged();
    }

    private class Async extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            String search;
            venuesAndCategories.addAll(FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories());
            for (int i =0; i < FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().size(); i++) {
                Logger.d("Category size " + FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories().size());
                if (FourSquareApp.get().getDatabase().venueCategoryDao().loadVenueAndCategories().get(i).getCategories() != null) {
                    Logger.d("Category true ");
                } else {
                    Logger.d("Category false ");
                }
            }

            if (venuesAndCategories != null && venuesAndCategories.size() > 0) {
//                    Custom SELECT
//                    venues.clear();
//                    venues.addAll(FourSquareApp.get().getDatabase().venueDao().findByName("Blue"));
                Logger.d("Category id " + venuesAndCategories.get(0).getCategories().get(0).getName());
            } else if (getIntent() != null && getIntent().getExtras() != null) {
                search = getIntent().getExtras().getString("search");
                RequestManager.search(search);
            } else {
                RequestManager.search(40.7, -74);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            barsAdapter.notifyDataSetChanged();
        }
    }
}
