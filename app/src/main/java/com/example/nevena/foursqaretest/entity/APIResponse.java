package com.example.nevena.foursqaretest.entity;

/**
 * Created by Nevena on 12/12/2016.
 */

public class APIResponse extends BaseEntity{
private APIResponse2 response;

    public APIResponse2 getResponse() {
        return response;
    }

    public void setResponse(APIResponse2 response) {
        this.response = response;
    }
}
