package com.example.nevena.foursqaretest.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by Nevena on 12/12/2016.
 */

@Entity(tableName = "category", foreignKeys = @ForeignKey(entity = Venue.class,
        parentColumns = "id",
        childColumns = "venueId", onDelete = ForeignKey.CASCADE), indices = {
        @Index(value = "venueId")
})
public class Category extends BaseEntity {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @Ignore
    private String id;
    @ColumnInfo(name = "venueId")
    private String venueId;
    @ColumnInfo(name = "name")
    private String name;
    @Embedded
    private Icon icon;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVenueId() {
        return venueId;
    }

    public void setVenueId(String venueId) {
        this.venueId = venueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

}
